﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
	public class Reporting
	{
		private Warehouse warehouse;

		public Reporting(Warehouse warehouse)
		{
			this.warehouse = warehouse;
		}

		public void RegisterIncoming(Product product, int quantity)
		{
			for (int i = 0; i < quantity; i++)
			{
				warehouse.AddProduct(product);
			}
		}

		public void RegisterOutgoing(Product product, int quantity)
		{
			for (int i = 0; i < quantity; i++)
			{
				warehouse.RemoveProduct(product);
			}
		}

		public void GenerateInventoryReport()
		{
			Console.WriteLine("Inventory Report:");
			warehouse.DisplayAllProducts();
		}
	}
}
