﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
	public class Product
	{
		public string Name { get; set; }
		public Money Price { get; set; }
		public List<Category> Categories { get; set; }

		public Product(string name, Money price, List<Category> categories)
		{
			Name = name;
			Price = price;
			Categories = categories;
		}
		public void DisplayProductInfo()
		{
			Console.WriteLine($"Product: {Name}");
			Console.WriteLine("Price:");
			Price.DisplayPrice();
			Console.WriteLine("Categories:");
			foreach (var category in Categories)
			{
				Console.WriteLine($"- {category.Name}: {category.Description}");
			}
		}
	}
}
