﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
	public class Currency
	{
		public string Name { get; set; }
		public string Symbol { get; set; }
		public double ExchangeRate { get; set; }

		public Currency(string name, string symbol, double exchangeRate)
		{
			Name = name;
			Symbol = symbol;
			ExchangeRate = exchangeRate;
		}

		public static void DisplayAllCurrencies(List<Currency> currencies)
		{
			foreach (var currency in currencies)
			{
				Console.WriteLine($"Currency: {currency.Name} ({currency.Symbol}), Exchange Rate: {currency.ExchangeRate}");
			}
		}
		public static void DisplayMostExpensiveCurrency(List<Currency> currencies)
		{
			Currency mostExpensive = currencies.OrderByDescending(c => c.ExchangeRate).First();
			Console.WriteLine($"The most expensive currency is: {mostExpensive.Name} ({mostExpensive.Symbol}), Exchange Rate: {mostExpensive.ExchangeRate}");
		}

	}
}
